#!/usr/bin/env python3
import os
import sys
import r2pipe
import subprocess
from zipfile import ZipFile


def extractAPK(apk,apktool,apktoolExtract,zipExtract): 
    print("\n[+] Extracting {} with {}".format(apk,apktool))
    extract = subprocess.check_output(["java", "-jar", apktool, "d", apk, "-o", apktoolExtract])
    print("\n[+] Extracting {} as a zip file".format(apk))
    with ZipFile(apk,"r") as zipObj:
        zipObj.extractall(zipExtract)
    
def manifestAnalysis(extractedDir):
    print("\n[+] Looking for AndroidManifest files in in {} and the permissions".format(extractedDir+"/apktool"))
    grep = subprocess.call(["find",extractedDir, "-name", "AndroidManifest.xml","-print", "-exec", "grep", "android.permission", "{}", ";"])

def findDex(zipExtract):
    list=[]
    for root, dirs, files in os.walk(zipExtract):
        for file in files:
            if file.endswith(".dex"):
                list.append(os.path.join(root,file))

    return list

def dexAnalysis(dexFiles):
    rootchecks = ["sudo","bin/su","superuser"]
    urls = ["http:","https:"]
    sqlStatements = ["SELECT","INSERT","DELETE","UPDATE","CREATE"]

    for dex in dexFiles:
        r = r2pipe.open(dex)
        print("\n[+] Basic information of {}".format(dex))
        print(r.cmd("iI"))
        for check in rootchecks:
            print("[+] looking for {} in {}".format(check,dex))
            print(r.cmd("izzq~+{}".format(check)))
        for protocol in urls:
            print("[+] looking for {} in {}".format(protocol,dex))
            print(r.cmd("izzq~+{}".format(protocol)))
        for statement in sqlStatements:
            print("[+] looking for {} in {}".format(statement,dex))
            print(r.cmd("izzq~{}".format(statement)))

def main():
    apktool = "/usr/local/bin/apktool.jar"
    outdir = "apk-extracted"

    apktoolExtract = outdir+"/apktool"
    zipExtract = outdir+"/zip"

    if os.path.isdir(outdir) and os.listdir(outdir) != []:
        print("[!] {} exists, remove before continuing".format(outdir))
        sys.exit()
    try:            
        apk = sys.argv[1]
        extractAPK(apk,apktool,apktoolExtract,zipExtract)
        manifestAnalysis(apktoolExtract)
        classesDex = findDex(zipExtract)
        dexAnalysis(classesDex)
    except IndexError:
        print("\n[!] Usage: {} file.apk".format(sys.argv[0]))
        sys.exit()    

if __name__ == '__main__':
    main()

