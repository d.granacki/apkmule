#+TITLE: APKMule
* Overview
  A simple script that uses [[https://ibotpeaches.github.io/Apktool/][apktool]] and [[https://www.radare.org/r/][radare2]] to extract and search the apk for low hanging fruit during static analysis.
* Requirements 
  The script requires that you have the following installed:
  - [[https://ibotpeaches.github.io/Apktool/][apktool]]
  - [[https://www.radare.org/r/][radare2]]
  - [[https://github.com/radare/radare2-r2pipe/tree/master/python][r2pipe]]
* Usage
  The script is written python3 and the =apk= file should be the first argument to the script.
  #+begin_src shell :results output :exports both
    python3 ./apkmule.py
  #+end_src

  #+RESULTS:
  : 
  : [!] Usage: ./apkmule.py file.apk
  
  The script does make an output directory that should be renamed or removed when running multipul times.
  #+begin_src shell :results output :exports both
    python3 ./apkmule.py ~/Downloads/Discord/Discord.apk
  #+end_src

  #+RESULTS:
  : [!] apk-extracted exists, remove before continuing

  After extracting the =apk= the script will search the =AndroidManifest.xml= and =classes.dex= file for low hanging fruit.
  #+begin_src shell :results output :exports both
    python3 ./apkmule.py ~/Downloads/Discord/Discord.apk
  #+end_src

  #+RESULTS:
  #+begin_example
  apk-extracted/apktool/AndroidManifest.xml
      <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
      <uses-permission android:name="android.permission.BLUETOOTH"/>
      <uses-permission android:name="android.permission.BROADCAST_STICKY"/>
      <uses-permission android:name="android.permission.INTERNET"/>
      <uses-permission android:name="android.permission.GET_ACCOUNTS"/>
      <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
      <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
      <uses-permission android:name="android.permission.CAMERA"/>
      <uses-permission android:name="android.permission.FOREGROUND_SERVICE"/>
      <uses-permission android:name="android.permission.RECORD_AUDIO"/>
      <uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS"/>
      <uses-permission android:name="android.permission.PACKAGE_USAGE_STATS"/>
      <uses-permission android:name="android.permission.VIBRATE"/>
      <uses-permission android:name="android.permission.WAKE_LOCK"/>
      <uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW"/>
      <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED"/>
	  <service android:exported="true" android:name="com.discord.app.DiscordConnectService" android:permission="com.discord.permission.CONNECT">
	  <receiver android:enabled="true" android:exported="true" android:name="com.discord.utilities.receiver.CampaignReceiver" android:permission="android.permission.INSTALL_PACKAGES">
	  <service android:directBootAware="false" android:enabled="@bool/enable_system_job_service_default" android:exported="true" android:name="androidx.work.impl.background.systemjob.SystemJobService" android:permission="android.permission.BIND_JOB_SERVICE"/>
	  <service android:enabled="true" android:exported="false" android:name="com.google.android.gms.analytics.AnalyticsJobService" android:permission="android.permission.BIND_JOB_SERVICE"/>
	  <receiver android:exported="true" android:name="com.google.firebase.iid.FirebaseInstanceIdReceiver" android:permission="com.google.android.c2dm.permission.SEND">
	  <receiver android:enabled="true" android:exported="true" android:name="com.google.android.gms.measurement.AppMeasurementInstallReferrerReceiver" android:permission="android.permission.INSTALL_PACKAGES">
	  <service android:enabled="true" android:exported="false" android:name="com.google.android.gms.measurement.AppMeasurementJobService" android:permission="android.permission.BIND_JOB_SERVICE"/>
  apk-extracted/apktool/original/AndroidManifest.xml

  [+] Extracting /home/jthorpe/Downloads/Discord/Discord.apk with /usr/local/bin/apktool.jar

  [+] Extracting /home/jthorpe/Downloads/Discord/Discord.apk as a zip file

  [+] Looking for AndroidManifest files in in apk-extracted/apktool/apktool and the permissions

  [+] Basic information of apk-extracted/zip/classes2.dex
  arch     dalvik
  baddr    0x0
  binsz    3833788
  bintype  class
  bits     32
  canary   false
  retguard false
  sanitiz  false
  class    035
  crypto   false
  endian   little
  havecode true
  laddr    0x0
  lang     dalvik
  linenum  false
  lsyms    false
  machine  Dalvik VM
  nx       false
  os       linux
  pic      false
  relocs   false
  static   true
  stripped false
  subsys   android
  va       false
  sha1  12-3833768c  968c0c2d1b8887ccdfe4520f5a977905dd2b8241
  adler32  12-3833788c  00000000

  [+] looking for sudo in apk-extracted/zip/classes2.dex

  [+] looking for bin/su in apk-extracted/zip/classes2.dex
  0x281fcb 16 15 /system/xbin/su

  [+] looking for superuser in apk-extracted/zip/classes2.dex
  0x281fb0 26 25 /system/app/Superuser.apk

  [+] looking for http: in apk-extracted/zip/classes2.dex
  0x285450 162 161 Analytics service at risk of not starting. For more reliable analytics, add the WAKE_LOCK permission to your manifest. See http://goo.gl/8Rd3yj for instructions.
  0x2855a2 170 169 AnalyticsReceiver is not registered or is disabled. Register the receiver for reliable dispatching on non-Google Play devices. See http://goo.gl/8Rd3yj for instructions.
  0x28564e 137 136 AnalyticsService is not registered or is disabled. Analytics service at risk of not starting. See http://goo.gl/8Rd3yj for instructions.
  0x2856d9 134 133 AnalyticsService not registered in the app manifest. Hits might not be delivered reliably. See http://goo.gl/8Rd3yj for instructions.
  0x28da5c 164 163 CampaignTrackingReceiver is not registered, not exported or is disabled. Installation campaign tracking is not possible. See http://goo.gl/8Rd3yj for instructions.
  0x29eccf 99 98 aHit delivery not possible. Missing network permissions. See http://goo.gl/8Rd3yj for instructions
  0x29fce2 308 307 IllegalStateException getting Ad Id Info. If you would like to see Audience reports, please ensure that you have added '<meta-data android:name="com.google.android.gms.version" android:value="@integer/google_play_services_version" />' to your application manifest file. See http://goo.gl/naFqQk for details.
  0x2df1b7 128 127 ~Missing required android.permission.ACCESS_NETWORK_STATE. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions
  0x2df237 116 115 rMissing required android.permission.INTERNET. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions
  0x31702f 6 5 http:
  0x317035 9 8 \ahttp://
  0x31703e 41 40 'http://schemas.android.com/apk/res-auto
  0x317067 44 43 *http://schemas.android.com/apk/res/android
  0x317094 32 31 http://www.google-analytics.com

  [+] looking for https: in apk-extracted/zip/classes2.dex
  0x2a1268 92 91 ZInvalid google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI. provided id
  0x2df124 79 78 MMissing google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI
  0x3170c5 10 9 \bhttps://
  0x3170d0 30 29 https://app-measurement.com/a
  0x3170ee 41 40 'https://e.crashlytics.com/spi/v2/events
  0x317118 27 26 https://google.com/search?
  0x317133 67 66 Ahttps://pagead2.googlesyndication.com/pagead/gen_204?id=gmob-apps
  0x317177 25 24 https://plus.google.com/
  0x317190 76 75 Jhttps://settings.crashlytics.com/spi/v2/platforms/android/apps/%s/settings
  0x3171dc 34 33  https://ssl.google-analytics.com
  0x3171ff 23 22 https://www.google.com
  0x317216 113 112 ohttps://www.googleadservices.com/pagead/conversion/app/deeplink?id_type=adid&sdk_version=%s&rdid=%s&bundleid=%s

  [+] looking for SELECT in apk-extracted/zip/classes2.dex
  0x2e67d1 15 14 SELECT * FROM 
  0x2e67e1 27 26 SELECT COUNT(*) FROM hits2
  0x2e67fc 60 59 :SELECT COUNT(1) FROM conditional_properties WHERE app_id=?
  0x2e6838 44 43 *SELECT COUNT(1) FROM queue WHERE rowid IN 
  0x2e6865 32 31 SELECT MAX(%s) FROM %s WHERE 1;
  0x2e6885 71 70 ESELECT hits_count FROM properties WHERE app_uid=? AND cid=? AND tid=?
  0x2e68cd 27 26 SELECTED_FOCUSED_STATE_SET
  0x2e68e8 36 35 "SELECTED_HOVERED_FOCUSED_STATE_SET
  0x2e690d 27 26 SELECTED_HOVERED_STATE_SET
  0x2e6929 27 26 SELECTED_PRESSED_STATE_SET
  0x2e6945 15 14 SELECTED_STATE
  0x2e6955 19 18 SELECTED_STATE_SET

  [+] looking for INSERT in apk-extracted/zip/classes2.dex

  [+] looking for DELETE in apk-extracted/zip/classes2.dex
  0x29350c 7 6 DELETE

  [+] looking for UPDATE in apk-extracted/zip/classes2.dex
  0x284207 29 28 \eAPI_VERSION_UPDATE_REQUIRED
  0x2e6a2d 32 31 SERVICE_VERSION_UPDATE_REQUIRED
  0x2ec25f 35 34 !TYPE_WINDOW_UPDATE length !=4: %s
  0x2f048f 75 74 IUPDATE queue SET retry_count = IFNULL(retry_count, 0) + 1 WHERE rowid IN 
  0x2f3a44 15 14 \rWINDOW_UPDATE
  0x2ffca6 40 39 &android.permission.UPDATE_DEVICE_STATS
  0x307131 62 61 <com.google.android.clockwork.home.UPDATE_ANDROID_WEAR_ACTION

  [+] looking for CREATE in apk-extracted/zip/classes2.dex
  0x28c378 159 158 CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL, '%s' TEXT NOT NULL, '%s' INTEGER);
  0x28c417 115 114 qCREATE TABLE IF NOT EXISTS app2 ( app_id TEXT NOT NULL, first_open_count INTEGER NOT NULL, PRIMARY KEY (app_id));
  0x28c48c 231 230 CREATE TABLE IF NOT EXISTS apps ( app_id TEXT NOT NULL, app_instance_id TEXT, gmp_app_id TEXT, resettable_device_id_hash TEXT, last_bundle_index INTEGER NOT NULL, last_bundle_end_timestamp INTEGER NOT NULL, PRIMARY KEY (app_id)) ;
  0x28c575 162 161 CREATE TABLE IF NOT EXISTS audience_filter_values ( app_id TEXT NOT NULL, audience_id INTEGER NOT NULL, current_results BLOB, PRIMARY KEY (app_id, audience_id));
  0x28c619 420 419 CREATE TABLE IF NOT EXISTS conditional_properties ( app_id TEXT NOT NULL, origin TEXT NOT NULL, name TEXT NOT NULL, value BLOB NOT NULL, creation_timestamp INTEGER NOT NULL, active INTEGER NOT NULL, trigger_event_name TEXT, trigger_timeout INTEGER NOT NULL, timed_out_event BLOB,triggered_event BLOB, triggered_timestamp INTEGER NOT NULL, time_to_live INTEGER NOT NULL, expired_event BLOB, PRIMARY KEY (app_id, name)) ;
  0x28c7bf 228 227 CREATE TABLE IF NOT EXISTS event_filters ( app_id TEXT NOT NULL, audience_id INTEGER NOT NULL, filter_id INTEGER NOT NULL, event_name TEXT NOT NULL, data BLOB NOT NULL, PRIMARY KEY (app_id, event_name, audience_id, filter_id));
  0x28c8a5 218 217 CREATE TABLE IF NOT EXISTS events ( app_id TEXT NOT NULL, name TEXT NOT NULL, lifetime_count INTEGER NOT NULL, current_bundle_count INTEGER NOT NULL, last_fire_timestamp INTEGER NOT NULL, PRIMARY KEY (app_id, name)) ;
  0x28c981 180 179 CREATE TABLE IF NOT EXISTS main_event_params ( app_id TEXT NOT NULL, event_id TEXT NOT NULL, children_to_process INTEGER NOT NULL, main_event BLOB NOT NULL, PRIMARY KEY (app_id));
  0x28ca37 213 212 CREATE TABLE IF NOT EXISTS properties ( app_uid INTEGER NOT NULL, cid TEXT NOT NULL, tid TEXT NOT NULL, params TEXT NOT NULL, adid INTEGER NOT NULL, hits_count INTEGER NOT NULL, PRIMARY KEY (app_uid, cid, tid)) ;
  0x28cb0e 237 236 CREATE TABLE IF NOT EXISTS property_filters ( app_id TEXT NOT NULL, audience_id INTEGER NOT NULL, filter_id INTEGER NOT NULL, property_name TEXT NOT NULL, data BLOB NOT NULL, PRIMARY KEY (app_id, property_name, audience_id, filter_id));
  0x28cbfb 118 117 tCREATE TABLE IF NOT EXISTS queue ( app_id TEXT NOT NULL, bundle_end_timestamp INTEGER NOT NULL, data BLOB NOT NULL);
  0x28cc73 170 169 CREATE TABLE IF NOT EXISTS raw_events ( app_id TEXT NOT NULL, name TEXT NOT NULL, timestamp INTEGER NOT NULL, metadata_fingerprint INTEGER NOT NULL, data BLOB NOT NULL);
  0x28cd1f 179 178 CREATE TABLE IF NOT EXISTS raw_events_metadata ( app_id TEXT NOT NULL, metadata_fingerprint INTEGER NOT NULL, metadata BLOB NOT NULL, PRIMARY KEY (app_id, metadata_fingerprint));
  0x28cdd4 170 169 CREATE TABLE IF NOT EXISTS user_attributes ( app_id TEXT NOT NULL, name TEXT NOT NULL, set_timestamp INTEGER NOT NULL, value BLOB NOT NULL, PRIMARY KEY (app_id, name)) ;


  [+] Basic information of apk-extracted/zip/classes.dex
  arch     dalvik
  baddr    0x0
  binsz    8576488
  bintype  class
  bits     32
  canary   false
  retguard false
  sanitiz  false
  class    035
  crypto   false
  endian   little
  havecode true
  laddr    0x0
  lang     dalvik
  linenum  false
  lsyms    false
  machine  Dalvik VM
  nx       false
  os       linux
  pic      false
  relocs   false
  static   true
  stripped false
  subsys   android
  va       false
  sha1  12-8576468c  403ab357dcab3b55d4cf13fee859f43afc31ec1b
  adler32  12-8576488c  00000000

  [+] looking for sudo in apk-extracted/zip/classes.dex

  [+] looking for bin/su in apk-extracted/zip/classes.dex

  [+] looking for superuser in apk-extracted/zip/classes.dex

  [+] looking for http: in apk-extracted/zip/classes.dex
  0x6cb7d5 6 5 http:
  0x6cb7db 9 8 \ahttp://
  0x6cb7e5 17 16 http://127.0.0.1
  0x6cb7f6 44 43 *http://schemas.android.com/apk/res/android
  0x6cb823 24 23 http://www.android.com/

  [+] looking for https: in apk-extracted/zip/classes.dex
  0x59e609 288 287 Event tracked before first activity resumed.\nIf it was triggered in the Application class, it might timestamp or even send an install long before the user opens the app.\nPlease check https://github.com/adjust/android_sdk#can-i-trigger-an-event-at-application-launch for more information.
  0x6c0272 54 51 2getString(R.string.guild…"https://discordapp.com")
  0x6cb84d 7 6 https:
  0x6cb854 10 9 \bhttps://
  0x6cb85f 27 26 https://account.samsung.cn
  0x6cb87a 29 28 \ehttps://account.samsung.com
  0x6cb897 29 28 \ehttps://api.spotify.com/v1/
  0x6cb8b5 23 22 https://app.adjust.com
  0x6cb8cc 41 40 'https://app.adjust.com/ndjczk?campaign=
  0x6cb8f6 27 26 https://cdn.discordapp.com
  0x6cb911 40 39 &https://cdn.discordapp.com/app-assets/
  0x6cb939 39 38 %https://cdn.discordapp.com/app-icons/
  0x6cb960 37 36 #https://cdn.discordapp.com/avatars/
  0x6cb985 37 36 #https://cdn.discordapp.com/banners/
  0x6cb9aa 43 42 )https://cdn.discordapp.com/channel-icons/
  0x6cb9d5 35 34 !https://cdn.discordapp.com/icons/
  0x6cb9f8 38 37 $https://cdn.discordapp.com/splashes/
  0x6cba1f 19 18 https://discord.gg
  0x6cba33 20 19 https://discord.gg/
  0x6cba47 36 35 "https://discord.gg/discord-testers
  0x6cba6c 21 20 https://discord.gift
  0x6cba82 22 21 https://discord.gift/
  0x6cba99 23 22 https://discordapp.com
  0x6cbab0 41 40 'https://discordapp.com/acknowledgements
  0x6cbad9 29 28 \ehttps://discordapp.com/api/
  0x6cbaf6 39 38 %https://discordapp.com/api//channels/
  0x6cbb1d 37 36 #https://discordapp.com/api//guilds/
  0x6cbb42 56 55 6https://discordapp.com/api//sso?service=zendesk&token=
  0x6cbb7a 36 35 "https://discordapp.com/api//users/
  0x6cbb9e 65 64 ?https://discordapp.com/api/v6/oauth2/samsung/authorize/callback
  0x6cbbe0 27 26 https://discordapp.com/app
  0x6cbbfb 40 39 &https://discordapp.com/billing/premium
  0x6cbc23 46 45 ,https://discordapp.com/developers/docs/intro
  0x6cbc51 35 34 !https://discordapp.com/guidelines
  0x6cbc74 38 37 $https://discordapp.com/login/handoff
  0x6cbc9a 36 35 "https://discordapp.com/store/skus/
  0x6cbcbe 54 53 4https://dl.discordapp.net/apps/android/versions.json
  0x6cbcf5 25 24 https://i.scdn.co/image/
  0x6cbd0f 31 30 https://ko.surveymonkey.com/r/
  0x6cbd2f 31 30 https://open.spotify.com/user/
  0x6cbd4e 44 43 *https://play.google.com/store/apps/details
  0x6cbd7a 63 62 =https://play.google.com/store/apps/details?id=com.authy.authy
  0x6cbdb9 59 58 9https://play.google.com/store/apps/details?id=com.discord
  0x6cbdf4 86 85 Thttps://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2
  0x6cbe4a 102 101 dhttps://play.google.com/store/apps/details?id=com.spotify.music&utm_source=discord&utm_medium=mobile
  0x6cbeb1 22 21 https://reddit.com/u/
  0x6cbec8 31 30 https://status.discordapp.com/
  0x6cbee7 38 37 $https://steamcommunity.com/profiles/
  0x6cbf0d 36 35 "https://support-dev.discordapp.com
  0x6cbf31 42 41 (https://support.apple.com/en-us/HT202039
  0x6cbf5c 31 30 https://support.discordapp.com
  0x6cbf7c 19 18 https://twitch.tv/
  0x6cbf90 31 30 https://twitter.com/discordapp
  0x6cbfb0 31 30 https://us.account.samsung.com
  0x6cbfd0 27 26 https://www.discordapp.com
  0x6cbfec 24 23 https://www.example.com
  0x6cc005 26 25 https://www.facebook.com/
  0x6cc01f 37 36 #https://www.facebook.com/discordapp
  0x6cc044 38 37 $https://www.instagram.com/discordapp
  0x6cc06b 23 22 https://www.paypal.com
  0x6cc083 32 31 https://www.surveymonkey.com/r/
  0x6cc0a4 25 24 https://www.twitter.com/
  0x6cc0be 29 28 https://youtube.com/channel/

  [+] looking for SELECT in apk-extracted/zip/classes.dex
  0x569b32 157 156  AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))
  0x582c90 42 41 (ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN
  0x582d80 35 34 !ACTION_ARGUMENT_SELECTION_END_INT
  0x582da3 37 36 #ACTION_ARGUMENT_SELECTION_START_INT
  0x582f1f 23 22 ACTION_CLEAR_SELECTION
  0x583494 15 14 \rACTION_SELECT
  0x5834a4 16 15 ACTION_SELECTOR
  0x5834ca 21 20 ACTION_SET_SELECTION
  0x5841c0 23 22 ARG_SELECTED_COLOR_KEY
  0x599f53 134 133 DELETE FROM room_table_modification_log WHERE version NOT IN( SELECT MAX(version) FROM room_table_modification_log GROUP BY table_id)
  0x599ffc 202 201 DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))
  0x59d3d5 38 37 $EXTRA_SELECTED_ACTION_PENDING_INTENT
  0x59ff04 17 16 FLIPPER_SELECTED
  0x5a6945 15 14 \rID_UNSELECTED
  0x5a7121 109 108 kINSERT INTO SystemIdInfo(work_spec_id, system_id) SELECT work_spec_id, alarm_id AS system_id FROM alarmInfo
  0x5a7498 116 115 rINSERT OR IGNORE INTO worktag(tag, work_spec_id) SELECT worker_class_name AS tag, id AS work_spec_id FROM workspec
  0x5a7a4d 23 22 INTENT_REGION_SELECTED
  0x5ab29c 26 25 LABEL_VISIBILITY_SELECTED
  0x6580a5 9 8 \aSELECT 
  0x6580af 17 16 SELECT * FROM ( 
  0x6580c0 49 48 /SELECT * FROM SystemIdInfo WHERE work_spec_id=?
  0x6580f1 84 83 RSELECT * FROM room_table_modification_log WHERE version  > ? ORDER BY version ASC;
  0x658145 38 37 $SELECT * FROM workspec WHERE id IN (
  0x65816b 35 34 !SELECT * FROM workspec WHERE id=?
  0x65818e 38 37 $SELECT * FROM workspec WHERE state=0
  0x6581b4 68 67 BSELECT * FROM workspec WHERE state=0 AND schedule_requested_at<>-1
  0x6581fa 173 172 SELECT * FROM workspec WHERE state=0 AND schedule_requested_at=-1 LIMIT (SELECT MAX(?-COUNT(*), 0) FROM workspec WHERE schedule_requested_at<>-1 AND state NOT IN (2, 3, 5))
  0x6582a7 79 78 MSELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'
  0x6582f7 24 23 SELECT COUNT(*) FROM ( 
  0x65830f 120 119 vSELECT COUNT(*)=0 FROM dependency WHERE work_spec_id=? AND prerequisite_id IN (SELECT id FROM workspec WHERE state!=2)
  0x658387 59 58 9SELECT COUNT(*)>0 FROM dependency WHERE prerequisite_id=?
  0x6583c2 55 54 5SELECT DISTINCT tag FROM worktag WHERE work_spec_id=?
  0x6583f9 70 69 DSELECT `tag`,`work_spec_id` FROM `WorkTag` WHERE `work_spec_id` IN (
  0x658440 24 23 SELECT id FROM workspec
  0x658458 54 53 4SELECT id FROM workspec WHERE state NOT IN (2, 3, 5)
  0x65848e 113 112 oSELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM workname WHERE name=?)
  0x6584ff 111 110 mSELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM worktag WHERE tag=?)
  0x65856e 93 92 [SELECT id, state FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)
  0x6585cb 54 53 4SELECT id, state, output FROM workspec WHERE id IN (
  0x658601 101 100 cSELECT id, state, output FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)
  0x658666 99 98 aSELECT id, state, output FROM workspec WHERE id IN (SELECT work_spec_id FROM worktag WHERE tag=?)
  0x6586c9 51 50 1SELECT id, state, output FROM workspec WHERE id=?
  0x6586fc 67 66 ASELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1
  0x65873f 103 102 eSELECT output FROM workspec WHERE id IN (SELECT prerequisite_id FROM dependency WHERE work_spec_id=?)
  0x6587a6 61 60 ;SELECT prerequisite_id FROM dependency WHERE work_spec_id=?
  0x6587e3 39 38 %SELECT state FROM workspec WHERE id=?
  0x65880a 61 60 ;SELECT work_spec_id FROM dependency WHERE prerequisite_id=?
  0x658847 48 47 .SELECT work_spec_id FROM workname WHERE name=?
  0x658877 46 45 ,SELECT work_spec_id FROM worktag WHERE tag=?
  0x6588a6 15 14 SELECTED_COLOR
  0x6588b6 19 18 SELECTED_STATE_SET
  0x6588ca 24 23 SELECTION_MODE_MULTIPLE
  0x6588e3 20 19 SELECTION_MODE_NONE
  0x6588f8 22 21 SELECTION_MODE_SINGLE
  0x65890f 16 15 SELECT_PROTOCOL
  0x658920 20 19 SELECT_PROTOCOL_ACK
  0x658935 26 25 SELECT_UPDATED_TABLES_SQL
  0x659940 24 23 STORE_GUILD_SELECTED_V5
  0x6599b2 27 26 STORE_SELECTED_CHANNELS_V6
  0x6630a5 34 33  TYPE_VIEW_TEXT_SELECTION_CHANGED
  0x667a58 12 11 \nUNSELECTED
  0x68655e 70 69 Dandroidx.browser.browseractions.extra.SELECTED_ACTION_PENDING_INTENT
  0x690f1a 38 37 $com.discord.actions.OVERLAY_SELECTOR
  0x6fde97 54 51 2selectedChannelSubject.m…it?.id ?: ID_UNSELECTED }

  [+] looking for INSERT in apk-extracted/zip/classes.dex
  0x5696eb 12 11 \n  INSERT: 
  0x5a711a 7 6 INSERT
  0x5a7121 109 108 kINSERT INTO SystemIdInfo(work_spec_id, system_id) SELECT work_spec_id, alarm_id AS system_id FROM alarmInfo
  0x5a718e 83 82 QINSERT OR IGNORE INTO `Dependency`(`work_spec_id`,`prerequisite_id`) VALUES (?,?)
  0x5a71e1 70 69 DINSERT OR IGNORE INTO `WorkName`(`name`,`work_spec_id`) VALUES (?,?)
  0x5a7229 555 554 INSERT OR IGNORE INTO `WorkSpec`(`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`period_start_time`,`minimum_retention_duration`,`schedule_requested_at`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
  0x5a7454 68 67 BINSERT OR IGNORE INTO `WorkTag`(`tag`,`work_spec_id`) VALUES (?,?)
  0x5a7498 116 115 rINSERT OR IGNORE INTO worktag(tag, work_spec_id) SELECT worker_class_name AS tag, id AS work_spec_id FROM workspec
  0x5a750c 80 79 NINSERT OR REPLACE INTO `SystemIdInfo`(`work_spec_id`,`system_id`) VALUES (?,?)
  0x5a755c 74 73 HINSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, "
  0x5a75a6 108 107 jINSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, "c84d23ade98552f1cec71088c1f0794c")
  0x5a7612 11 10 \tINSERTION
  0x6775d0 74 73 H` BEGIN INSERT OR REPLACE INTO room_table_modification_log VALUES(null, 

  [+] looking for DELETE in apk-extracted/zip/classes.dex
  0x56966d 12 11 \n  DELETE: 
  0x582e58 22 21 ACTION_CHANNEL_DELETE
  0x582e90 32 31 ACTION_CHANNEL_OVERWRITE_DELETE
  0x583005 20 19 ACTION_EMOJI_DELETE
  0x583119 21 20 ACTION_INVITE_DELETE
  0x5831f0 22 21 ACTION_MESSAGE_DELETE
  0x5833af 19 18 ACTION_ROLE_DELETE
  0x5835d9 22 21 ACTION_WEBHOOK_DELETE
  0x58e472 13 12 \vCALL_DELETE
  0x58ebd9 29 28 CHANGE_KEY_PRUNE_DELETE_DAYS
  0x58edb2 15 14 CHANNEL_DELETE
  0x58fb4f 343 342 CREATE TABLE IF NOT EXISTS `Dependency` (`work_spec_id` TEXT NOT NULL, `prerequisite_id` TEXT NOT NULL, PRIMARY KEY(`work_spec_id`, `prerequisite_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE , FOREIGN KEY(`prerequisite_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )
  0x58fca8 226 225 CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )
  0x58fd8c 222 221 CREATE TABLE IF NOT EXISTS `WorkName` (`name` TEXT NOT NULL, `work_spec_id` TEXT NOT NULL, PRIMARY KEY(`name`, `work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )
  0x5901eb 219 218 CREATE TABLE IF NOT EXISTS `WorkTag` (`tag` TEXT NOT NULL, `work_spec_id` TEXT NOT NULL, PRIMARY KEY(`tag`, `work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )
  0x599e90 7 6 DELETE
  0x599e97 14 13 \fDELETE FROM 
  0x599ea5 47 46 -DELETE FROM SystemIdInfo where work_spec_id=?
  0x599ed5 25 24 DELETE FROM `Dependency`
  0x599eef 27 26 DELETE FROM `SystemIdInfo`
  0x599f0b 23 22 DELETE FROM `WorkName`
  0x599f23 23 22 DELETE FROM `WorkSpec`
  0x599f3b 22 21 DELETE FROM `WorkTag`
  0x599f53 134 133 DELETE FROM room_table_modification_log WHERE version NOT IN( SELECT MAX(version) FROM room_table_modification_log GROUP BY table_id)
  0x599fda 32 31 DELETE FROM workspec WHERE id=?
  0x599ffc 202 201 DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))
  0x59a0c6 103 102 eDELETE FROM workspec WHERE state IN (2, 3, 5) AND (period_start_time + minimum_retention_duration) < 
  0x59a12d 13 12 \vDELETE.java
  0x59a13a 9 8 \aDELETED
  0x59a144 24 23 DELETE_CONNECTION_MODAL
  0x5a4154 14 13 \fGUILD_DELETE
  0x5a4233 18 17 GUILD_ROLE_DELETE
  0x5a7c10 26 25 INVALID_BULK_DELETE_COUNT
  0x647329 15 14 MESSAGE_DELETE
  0x647339 20 19 MESSAGE_DELETE_BULK
  0x654cfc 22 21 RESULT_EXTRA_UNDELETE
  0x65897f 23 22 SEMANTIC_ACTION_DELETE
  0x690e8b 45 44 +com.discord.NOTIFICATION_DELETED_CHANNEL_ID
  0x6910c7 48 47 .com.discord.intent.action.NOTIFICATION_DELETED

  [+] looking for UPDATE in apk-extracted/zip/classes.dex
  0x56976b 12 11 \n  UPDATE: 
  0x569a75 22 21  -- LAST 20 UPDATES:\n
  0x582eb1 32 31 ACTION_CHANNEL_OVERWRITE_UPDATE
  0x582ed2 22 21 ACTION_CHANNEL_UPDATE
  0x58301a 20 19 ACTION_EMOJI_UPDATE
  0x583086 20 19 ACTION_GUILD_UPDATE
  0x58312f 21 20 ACTION_INVITE_UPDATE
  0x5831bf 26 25 ACTION_MEMBER_ROLE_UPDATE
  0x5831da 21 20 ACTION_MEMBER_UPDATE
  0x5833c3 19 18 ACTION_ROLE_UPDATE
  0x5835f0 22 21 ACTION_WEBHOOK_UPDATE
  0x583f77 26 25 APPLICATIONS_STORE_UPDATE
  0x58e4ef 13 12 \vCALL_UPDATE
  0x58ee31 15 14 CHANNEL_UPDATE
  0x58fb4f 343 342 CREATE TABLE IF NOT EXISTS `Dependency` (`work_spec_id` TEXT NOT NULL, `prerequisite_id` TEXT NOT NULL, PRIMARY KEY(`work_spec_id`, `prerequisite_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE , FOREIGN KEY(`prerequisite_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )
  0x58fca8 226 225 CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )
  0x58fd8c 222 221 CREATE TABLE IF NOT EXISTS `WorkName` (`name` TEXT NOT NULL, `work_spec_id` TEXT NOT NULL, PRIMARY KEY(`name`, `work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )
  0x5901eb 219 218 CREATE TABLE IF NOT EXISTS `WorkTag` (`tag` TEXT NOT NULL, `work_spec_id` TEXT NOT NULL, PRIMARY KEY(`tag`, `work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )
  0x59f645 24 23 FLAG_ADAPTER_FULLUPDATE
  0x59fe68 13 12 \vFLAG_UPDATE
  0x5a0250 20 19 FULLUPDATE_PAYLOADS
  0x5a4163 20 19 GUILD_EMOJIS_UPDATE
  0x5a4182 26 25 GUILD_INTEGRATIONS_UPDATE
  0x5a41c4 25 24 GUILD_MEMBER_LIST_UPDATE
  0x5a41f3 20 19 GUILD_MEMBER_UPDATE
  0x5a4246 18 17 GUILD_ROLE_UPDATE
  0x5a4285 14 13 \fGUILD_UPDATE
  0x6470e3 17 16 MAX_UPDATE_COUNT
  0x647406 15 14 MESSAGE_UPDATE
  0x65182c 26 25 POST_UPDATES_ON_ANIMATION
  0x651b96 16 15 PRESENCE_UPDATE
  0x652d0b 54 51 2PendingIntent.getActivit…tent.FLAG_UPDATE_CURRENT)
  0x652d41 54 51 2PendingIntent.getBroadca…tent.FLAG_UPDATE_CURRENT)
  0x654e00 23 22 RESULT_PROGRESS_UPDATE
  0x658935 26 25 SELECT_UPDATED_TABLES_SQL
  0x658b8c 15 14 SESSION_UPDATE
  0x6592d4 27 26 SPEAKING_UPDATES_BUFFER_MS
  0x661aab 34 33  TRACE_HANDLE_ADAPTER_UPDATES_TAG
  0x667b07 7 6 UPDATE
  0x667b0e 9 8 \aUPDATE 
  0x667b17 41 40 'UPDATE workspec SET output=? WHERE id=?
  0x667b40 52 51 2UPDATE workspec SET period_start_time=? WHERE id=?
  0x667b74 52 51 2UPDATE workspec SET run_attempt_count=0 WHERE id=?
  0x667ba8 70 69 DUPDATE workspec SET run_attempt_count=run_attempt_count+1 WHERE id=?
  0x667bee 75 74 IUPDATE workspec SET schedule_requested_at=-1 WHERE state NOT IN (2, 3, 5)
  0x667c39 128 127 ~UPDATE workspec SET schedule_requested_at=0 WHERE state NOT IN (2, 3, 5) AND schedule_requested_at=-1 AND interval_duration<>0
  0x667cb9 56 55 6UPDATE workspec SET schedule_requested_at=? WHERE id=?
  0x667cf2 27 26 UPDATE workspec SET state=
  0x667d0d 69 68 CUPDATE workspec SET state=0, schedule_requested_at=-1 WHERE state=1
  0x667d53 18 17 UPDATE_ITEM_COUNT
  0x667d65 14 13 \fUPDATE_RANGE
  0x667d74 18 17 UPDATE_TABLE_NAME
  0x668173 24 23 USER_CONNECTIONS_UPDATE
  0x66819a 27 26 USER_GUILD_SETTINGS_UPDATE
  0x6681cc 17 16 USER_NOTE_UPDATE
  0x668205 29 28 \eUSER_PAYMENT_SOURCES_UPDATE
  0x66822e 29 28 \eUSER_REQUIRED_ACTION_UPDATE
  0x66824c 21 20 USER_SETTINGS_UPDATE
  0x66826e 26 25 USER_SUBSCRIPTIONS_UPDATE
  0x668288 13 12 \vUSER_UPDATE
  0x66bf7d 20 19 VOICE_SERVER_UPDATE
  0x66bf92 19 18 VOICE_STATE_UPDATE
  0x66d67a 35 34 !WORKSPEC_ADD_TRIGGER_UPDATE_DELAY
  0x684443 46 45 ,android.media.ACTION_SCO_AUDIO_STATE_UPDATED

  [+] looking for CREATE in apk-extracted/zip/classes.dex
  0x582e41 22 21 ACTION_CHANNEL_CREATE
  0x582e6f 32 31 ACTION_CHANNEL_OVERWRITE_CREATE
  0x582ff0 20 19 ACTION_EMOJI_CREATE
  0x583103 21 20 ACTION_INVITE_CREATE
  0x58339b 19 18 ACTION_ROLE_CREATE
  0x5835c2 22 21 ACTION_WEBHOOK_CREATE
  0x5836c3 17 16 ACTIVITY_CREATED
  0x58e465 13 12 \vCALL_CREATE
  0x58e520 37 36 #CANNOT_CREATE_VERIFICATION_DATABASE
  0x58eda2 15 14 CHANNEL_CREATE
  0x58f9ac 7 6 CREATE
  0x58f9b3 86 85 TCREATE  INDEX `index_Dependency_prerequisite_id` ON `Dependency` (`prerequisite_id`)
  0x58fa09 80 79 NCREATE  INDEX `index_Dependency_work_spec_id` ON `Dependency` (`work_spec_id`)
  0x58fa59 76 75 JCREATE  INDEX `index_WorkName_work_spec_id` ON `WorkName` (`work_spec_id`)
  0x58faa5 94 93 \\CREATE  INDEX `index_WorkSpec_schedule_requested_at` ON `WorkSpec` (`schedule_requested_at`)
  0x58fb03 74 73 HCREATE  INDEX `index_WorkTag_work_spec_id` ON `WorkTag` (`work_spec_id`)
  0x58fb4f 343 342 CREATE TABLE IF NOT EXISTS `Dependency` (`work_spec_id` TEXT NOT NULL, `prerequisite_id` TEXT NOT NULL, PRIMARY KEY(`work_spec_id`, `prerequisite_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE , FOREIGN KEY(`prerequisite_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )
  0x58fca8 226 225 CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )
  0x58fd8c 222 221 CREATE TABLE IF NOT EXISTS `WorkName` (`name` TEXT NOT NULL, `work_spec_id` TEXT NOT NULL, PRIMARY KEY(`name`, `work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )
  0x58fe6c 893 892 CREATE TABLE IF NOT EXISTS `WorkSpec` (`id` TEXT NOT NULL, `state` INTEGER NOT NULL, `worker_class_name` TEXT NOT NULL, `input_merger_class_name` TEXT, `input` BLOB NOT NULL, `output` BLOB NOT NULL, `initial_delay` INTEGER NOT NULL, `interval_duration` INTEGER NOT NULL, `flex_duration` INTEGER NOT NULL, `run_attempt_count` INTEGER NOT NULL, `backoff_policy` INTEGER NOT NULL, `backoff_delay_duration` INTEGER NOT NULL, `period_start_time` INTEGER NOT NULL, `minimum_retention_duration` INTEGER NOT NULL, `schedule_requested_at` INTEGER NOT NULL, `required_network_type` INTEGER, `requires_charging` INTEGER NOT NULL, `requires_device_idle` INTEGER NOT NULL, `requires_battery_not_low` INTEGER NOT NULL, `requires_storage_not_low` INTEGER NOT NULL, `trigger_content_update_delay` INTEGER NOT NULL, `trigger_max_content_delay` INTEGER NOT NULL, `content_uri_triggers` BLOB, PRIMARY KEY(`id`))
  0x5901eb 219 218 CREATE TABLE IF NOT EXISTS `WorkTag` (`tag` TEXT NOT NULL, `work_spec_id` TEXT NOT NULL, PRIMARY KEY(`tag`, `work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )
  0x5902c6 90 89 XCREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)
  0x590320 108 107 jCREATE TEMP TABLE room_table_modification_log(version INTEGER PRIMARY KEY AUTOINCREMENT, table_id INTEGER)
  0x59038c 36 35 "CREATE TEMP TRIGGER IF NOT EXISTS 
  0x5903b0 9 8 \aCREATED
  0x5903b9 42 41 (CREATE_FROM_FAMILIES_WITH_DEFAULT_METHOD
  0x5903e4 22 21 CREATE_INSTANT_INVITE
  0x5903fa 14 13 \fCREATE_QUERY
  0x590409 22 21 CREATE_SYSTEM_ID_INFO
  0x590420 25 24 CREATE_VERSION_TABLE_SQL
  0x5a4146 14 13 \fGUILD_CREATE
  0x5a4220 18 17 GUILD_ROLE_CREATE
  0x5a68f3 11 10 \tID_CREATE
  0x5a7a3c 16 15 INTENT_RECREATE
  0x647319 15 14 MESSAGE_CREATE
  0x647a6d 13 12 \vMODE_CREATE
  0x647a7b 24 23 MODE_CREATE$annotations
  0x64e3b0 11 10 \tON_CREATE
  0x654fcf 42 41 (ROOM_CANNOT_CREATE_VERIFICATION_DATABASE
  0x661a95 22 21 TRACE_CREATE_VIEW_TAG
  0x66280d 13 12 \vTYPE_CREATE
  0x662b10 20 19 TYPE_MESSAGE_CREATE
  0x6e7bca 29 28 \emovefrom ACTIVITY_CREATED: 
  0x6e7be8 19 18 movefrom CREATED: 
  0x6e7c3b 26 25 moveto ACTIVITY_CREATED: 
  0x6e7c56 17 16 moveto CREATED: 

  #+end_example
